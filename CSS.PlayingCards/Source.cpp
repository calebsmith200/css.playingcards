//C++ Playing Cards
//Caleb Smith

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit
{
	SPADE,
	CLUB,
	HEART,
	DIAMOND
};

enum Rank
{
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14

};

struct Card
{
	Suit suit;
	Rank rank;
};

int main()
{
	_getch();
	return 0;
}